# Overview
This is a Snakemake workflow designed to run OrthoFinder on AWS.

# Quickstart
The following script provides an example command that should prep files for execution and execute this snakemake workflow.
```
# get some proteins on aws
aws s3 cp myproteins1.fasta s3://salk-tm-dev/workspace/
aws s3 cp myproteins2.fasta s3://salk-tm-dev/workspace/


# login to headnode, navigate to worfklows/snake_orthofinder/, activate snake,
# then execute a snakemake command to run on your data using tibanna
nohup snakemake -p -j 1 --use-conda --tibanna \
    --snakefile Snakefile \
    --default-remote-prefix salk-tm-dev/workspace/ \
    --default-resources mem_mb=30000 disk_mb=1000000 \
    --tibanna-config root_ebs_size=16 log_bucket=salk-tm-logs \
    --config \
        proteins=["myproteins1.fasta","myproteins2.fasta"] \
        outbase=myorthofinder_results \
        threads=16 \
    > test.ortho.log 2>&1 &
```

# Config Fields
| Config | Description |
| ------ | ------ |
| _proteins_ | A  list of paths to protein fasta files **REQUIRED** |
| _outbase_ | A name prefix for your output file. **REQUIRED** |
| _threads_ | how many threads/cores to use when running orthofinder. **REQUIRED** |
