from snakemake.utils import validate

import json
validate(config, "schema/config.schema.json")
print(json.dumps(config, indent=4))

ORTHO_WKDIR = config["outbase"] + ".orthofinder.dmnd_dbs"
ORTHO_BLASTDIR = config["outbase"] + ".orthofinder.blasts"
ORTHO_OUTDIR = config["outbase"] + ".orthofinder"
ORTHO_OUTPUTS = [
    "Comparative_Genomics_Statistics",
    "Gene_Duplication_Events",
    "Gene_Trees",
    "Orthogroup_Sequences",
    "Orthogroups",
    "Orthologues",
    "Phylogenetic_Hierarchical_Orthogroups",
    "Phylogenetically_Misplaced_Genes",
    "Putative_Xenologs",
    "Resolved_Gene_Trees",
    "Single_Copy_Orthologue_Sequences",
    "Species_Tree",
    "WorkingDirectory"
]

targ = {
    (False, False): f"{ORTHO_OUTDIR}/Orthogroups.tar.gz",
    (False, True): f"{ORTHO_OUTDIR}.tar.gz",
    (True, False): f"{ORTHO_OUTDIR}.single.tar.gz",
    (True, True): f"{ORTHO_OUTDIR}.single.tar.gz"
}[(config["single"], config["safe_pack"])]


rule all:
    input:
        targ


rule orthofinder_single:
    input:
        config["proteins"]
    output:
        f"{ORTHO_OUTDIR}.single.tar.gz"
    params:
        prot_dir = "proteins",
        outdir = ORTHO_OUTDIR,
        opt = config["ortho_opt"],
        ulimit = max(8192, (len(config["proteins"]) + 2 ) ** 2)
    conda:
        "envs/env.yml"
    threads:
        config["threads"]
    shell:
        """
            mkdir {params.prot_dir}
            mv {input} {params.prot_dir}
            orthofinder -f {params.prot_dir} -t {threads} -a {threads} {params.opt}
            mkdir -p {params.outdir}
            mv {params.prot_dir}/OrthoFinder/Results*/* {params.outdir}
            tar -zcf {params.outdir}.single.tar.gz {params.outdir}
        """



rule orthofinder_prep:
    input:
        config["proteins"]
    output:
        ORTHO_WKDIR + ".tar.gz"
    params:
        prot_dir = "proteins",
        workdir = ORTHO_WKDIR
    conda:
        "envs/env.yml"
    threads:
        config["threads"]
    group:
        "orthofinder_prep"
    shell:
        """
        mkdir -p {params.workdir}
        mkdir {params.prot_dir}
        mv {input} {params.prot_dir}
        orthofinder -f {params.prot_dir} -op
        mv {params.prot_dir}/OrthoFinder/Results_*/WorkingDirectory/* {params.workdir}/
        tar -zcf {output} {params.workdir}/
        """


rule orthofinder_blast:
    input:
        lambda wcs: rules.orthofinder_prep.output[0]
    output:
        ORTHO_BLASTDIR + "/Blast{i}.tar.gz"
    params:
        outdir = ORTHO_BLASTDIR,
        workdir = ORTHO_WKDIR,
        nprots = len(config["proteins"]) - 1
    conda:
        "envs/env.yml"
    threads:
        config["threads"]
    group:
        lambda wcs: f"orthofinder_blast_{wcs['i']}"
    shell:
        """
        mkdir -p {params.outdir}
        tar -zxf {input}
        for j in {{0..{params.nprots}}}
        do
            diamond blastp \\
                -d {params.workdir}/diamondDBSpecies$j \\
                -q {params.workdir}/Species{wildcards.i}.fa \\
                -o {params.outdir}/Blast{wildcards.i}_$j.txt \\
                --more-sensitive -p {threads} --quiet -e 0.001 --compress 1
        done
        tar -zcf Blast{wildcards.i}.tar.gz {params.outdir}
        mv Blast{wildcards.i}.tar.gz {params.outdir}
        """


rule orthofinder_merge:
    input:
        prep = rules.orthofinder_prep.output[0],
        blasts = [rules.orthofinder_blast.output[0].format(i=i) for i, _ in enumerate(config["proteins"])]
    output:
        [f"{ORTHO_OUTDIR}/{f}.tar.gz" for f in ORTHO_OUTPUTS]
    params:
        blastdir = ORTHO_BLASTDIR,
        workdir = ORTHO_WKDIR,
        outdir = ORTHO_OUTDIR,
        opt = config["ortho_opt"],
        ulimit = max(8192, (len(config["proteins"]) + 2 ) ** 2)
    conda:
        "envs/env.yml"
    threads:
        config["threads"]
    shell:
        """
            tar -zxf {input.prep}
            ls {params.blastdir} | grep .tar.gz > blasttars.txt
            parallel -j {threads} tar -zxf {params.blastdir}/{{1}} :::: blasttars.txt 
            for f in {params.blastdir}/*.txt.gz; do mv $f {params.workdir}/ ; done
            ls {params.workdir} | grep .txt.gz > blasttxts.txt
            parallel -j {threads} gunzip {params.workdir}/{{1}} :::: blasttxts.txt
            ulimit -n {params.ulimit}
            orthofinder -b {params.workdir} -t {threads} -a {threads} {params.opt}
            mkdir -p {params.outdir}
            cd {params.workdir}/OrthoFinder/Results*/
            for f in $(ls . | grep -v .txt) ; do tar -zcf ../../../{params.outdir}/$f.tar.gz $f ; done
        """


rule orthofinder_safe_merge:
    input:
        prep = rules.orthofinder_prep.output[0],
        blasts = [rules.orthofinder_blast.output[0].format(i=i) for i, _ in enumerate(config["proteins"])]
    output:
        f"{ORTHO_OUTDIR}.tar.gz"
    params:
        blastdir = ORTHO_BLASTDIR,
        workdir = ORTHO_WKDIR,
        outdir = ORTHO_OUTDIR,
        opt = config["ortho_opt"],
        ulimit = max(8192, (len(config["proteins"]) + 2 ) ** 2)
    conda:
        "envs/env.yml"
    threads:
        config["threads"]
    shell:
        """
            tar -zxf {input.prep}
            ls {params.blastdir} | grep .tar.gz > blasttars.txt
            parallel -j {threads} tar -zxf {params.blastdir}/{{1}} :::: blasttars.txt 
            for f in {params.blastdir}/*.txt.gz; do mv $f {params.workdir}/ ; done
            ls {params.workdir} | grep .txt.gz > blasttxts.txt
            parallel -j {threads} gunzip {params.workdir}/{{1}} :::: blasttxts.txt
            ulimit -n {params.ulimit}
            orthofinder -b {params.workdir} -t {threads} -a {threads} {params.opt}
            mkdir -p {params.outdir}
            mv {params.workdir}/OrthoFinder/Results*/* {params.outdir}
            tar -zcf {params.outdir}.tar.gz {params.outdir}
        """


rule emapper:
    input:
        config['proteins']
    output:
        config['outbase'] + ".emapper.tar.gz"
    params:
        outbase = config['outbase'] + ".emapper",
        prot_dir = "proteins_dir",
        data_dir = "emapper_data"
    conda:
        "envs/env.yml"
    threads:
        config['threads']
    group:
        "empaper_" + config['outbase']
    shell:
        """
        mkdir {params.prot_dir}
        tar -xzf {input} -C {params.prot_dir}
        mkdir -p {params.data_dir}
        download_eggnog_data.py -y --data_dir {params.data_dir}
        mkdir {params.outbase}
        for f in $(ls {params.prot_dir}) ; do \\
            emapper.py --dbmem -i {params.prot_dir}/$f -o {params.outbase}/$f.emapper -m diamond --cpu {threads} --data_dir {params.data_dir} \\
            ; done
        tar -zcf {output} {params.outbase}
        """

