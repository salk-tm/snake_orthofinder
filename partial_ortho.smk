


rule orthofinder_merge:
    input:
        config['partial']
    output:
        config['output']
    params:
        opt = "",
        ulimit = 100000
    conda:
        "envs/env.yml"
    threads:
        config["threads"]
    shell:
        """
            tar -zxf {input}
            ulimit -n {params.ulimit}
            orthofinder -fg OrthoFinder/Results*/ {params.opt}
            tar -zcf {output} Orthofinder/
        """

